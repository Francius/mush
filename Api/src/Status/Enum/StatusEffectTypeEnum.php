<?php

namespace Mush\Status\Enum;

class StatusEffectTypeEnum
{
    public const MISC = 'misc_status';
    public const DISEASE = 'disease';
    public const INJURY = 'injury';
    public const DISORDER = 'disorder';
}

<?php

namespace Mush\Player\Enum;

class ModifierScopeEnum
{
    public const ACTION_TECHNICIAN = 'action_technician';
    public const ACTION_SHOOT = 'action_shoot';
    public const ACTION_REINFORCE = 'action_reinforce';
    public const ACTION_ATTACK = 'action_attack';

    public const EVENT_CLUMSINESS = 'event_clumsiness';
    public const EVENT_DIRTY = 'event_dirty';
    public const EVENT_ACTION_POINT_CONVERSION = 'event_clumsiness';
}

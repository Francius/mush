<?php

namespace Mush\Player\Enum;

class ModifierTargetEnum
{
    public const ACTION_POINT = 'action_point';
    public const MOVEMENT_POINT = 'movement_point';
    public const HEALTH_POINT = 'health_point';
    public const MORAL_POINT = 'moral_point';
    public const SATIETY = 'satiety';

    public const PERCENTAGE = 'percentage';
}

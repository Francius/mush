<?php

namespace Mush\Place\Enum;

class PlaceTypeEnum
{
    public const ROOM = 'room';
    public const GREAT_BEYOND = 'great_beyond';
}

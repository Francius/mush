<?php

namespace Mush\Daedalus\Enum;

class AlertEnum
{
    public const LOW_OXYGEN = 'low.oxygen';
    public const LOW_HULL = 'low.hull';
    public const NUMBER_FIRE = 'number.fire';
    public const BROKEN_DOORS = 'broken.doors';
    public const BROKEN_EQUIPMENTS = 'broken.equipments';
    public const NO_GRAVITY = 'no.gravity';
    public const LOST_CREWMEMBER = 'lost.crewmember';
    public const PARIA_CREWMEMBER = 'paria.crewmember';
    public const NO_ALERT = 'no.alert';
    public const HUNGER_GENERAL = 'hunger.general';
    public const LOST_COMMUNICATION = 'lost.communication';
    public const HUNTERS_ATTACK = 'hunters.attack';
}

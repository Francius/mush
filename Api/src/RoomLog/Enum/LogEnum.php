<?php

namespace Mush\RoomLog\Enum;

class LogEnum
{
    public const GAIN_TRIUMPH = 'gain_triumph';
    public const AWAKEN = 'awaken';
    public const DEATH = 'death';
    public const GAIN_ACTION_POINT = 'gain_action_point';
    public const LOSS_ACTION_POINT = 'loss_action_point';
    public const GAIN_MOVEMENT_POINT = 'gain_movement_point';
    public const LOSS_MOVEMENT_POINT = 'loss_movement_point';
    public const GAIN_HEALTH_POINT = 'gain_health_point';
    public const LOSS_HEALTH_POINT = 'loss_health_point';
    public const GAIN_MORAL_POINT = 'gain_moral_point';
    public const LOSS_MORAL_POINT = 'loss_moral_point';
    public const OBJECT_FELT = 'object_felt';
    public const CLUMSINESS = 'clumsiness';
    public const CLUMSINESS_PREVENTED = 'clumsiness_prevented';
    public const SOILED = 'soiled';
    public const SOIL_PREVENTED = 'soil_prevented';
    public const SOIL_PREVENTED_OCD = 'soil_prevented_ocd';
    public const OXY_LOW_USE_CAPSULE = 'oxy_low_use_capsule';
    public const TREMOR_NO_GRAVITY = 'tremor_no_gravity';
    public const TREMOR_GRAVITY = 'tremor_gravity';
    public const ELECTRIC_ARC = 'electric_arc';
    public const METAL_PLATE = 'metal_plate';
    public const PANIC_CRISIS = 'panic_crisis';
    public const EQUIPMENT_DESTROYED = 'equipment_destroyed';
    public const EQUIPMENT_BROKEN = 'equipment_broken';
}
